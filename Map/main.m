//
//  main.m
//  Map
//
//  Created by 汪洋 on 2017/5/11.
//  Copyright © 2017年 汪洋. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
