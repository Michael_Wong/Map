//
//  AppDelegate.h
//  Map
//
//  Created by 汪洋 on 2017/5/11.
//  Copyright © 2017年 汪洋. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

